#include <iostream>
using namespace std;

// ����� ��� ������������
class Massiv
{

	int *p;
	int n;
public:
	bool isEmpty();
	Massiv();
	Massiv(int *pp, int nn);
	Massiv(Massiv &M);
	int & operator[](int i) const;
	Massiv& operator = (const Massiv& Ob);
	Massiv& operator = (Massiv&& Ob);
	// ����������� �����������
	Massiv::Massiv(Massiv&& M);  // �������� - �������������� ������
	~Massiv(); // �������� �����������
	void print();
};
Massiv::~Massiv()
{
	if (p != nullptr) delete[]p;
	//std::cout << "\nDestryctor";
}
Massiv::Massiv(Massiv & M)
{ // ����� ����������� �����������
  //printf("\nConstr Copy");
	n = M.n;
	p = new int[n];
	for (int i = 0; i<n; i++)
		p[i] = M.p[i];
}

bool Massiv::isEmpty()
{
	if (p == nullptr) return true;
	else return false;
}

Massiv& Massiv::operator = (const Massiv& Ob)
{
	if (&Ob == this) return *this;
	if (p != nullptr) delete[] p;
	n = Ob.n;
	p = new int[n];
	for (int i = 0; i<n; i++)
		p[i] = Ob.p[i];
	return *this;
}

Massiv& Massiv::operator = (Massiv&& Ob)
{
	if (&Ob == this) return *this;
	n = Ob.n;
	p = Ob.p;
	Ob.n = 0;
	Ob.p = nullptr;
	return *this;
}

// ����������� �����������
Massiv::Massiv(Massiv&& M)  // �������� - �������������� ������
	: p(M.p), n(M.n)   // ����� ���������������� ���
{
	//std::cout << "\nConstr Move";
	//p = M.p;  // ��� ���������������� ���
	//n = M.n;
	// ��������� ������-������ ��������� ������� �������� �� ���������. ��� �� ��������� ����������� ����������� ����������� ������
	M.p = nullptr;
	M.n = 0;
}

Massiv::Massiv(int *pp, int nn)
{
	//std::cout << "\nConstr";
	n = nn;
	p = new int[n];
	for (int i = 0; i<n; i++) p[i] = pp[i];
}

void Massiv::print()
{
	std::cout << "\n";
	for (int i = 0; i < n; i++)
		//printf("%d  ", p[i]);
		std::cout << p[i] << " ";
}
int & Massiv::operator[](int i) const
{
	if (p == nullptr)
	{
		throw out_of_range("Object is null!!!!!");
	}
	if (i<0 || i >= n) throw out_of_range("out_of_range Massiv");
	return p[i];
}

Massiv::Massiv()
{
	p = nullptr;
	n = 0;
}

template <typename T>
class mystack
{
	class StackItem
	{
	public:
		T item;
		StackItem *pNext = nullptr;

		StackItem(const T & val)
		{
			item = val;
		}
		StackItem(T && val)
		{
			item = move(val);
		}
		~StackItem()
		{
			delete item;
		}

	};

	StackItem *pFirst = nullptr;
public:
	void push(T&& value)
	{
		StackItem *pItem = new StackItem(move(value));
		pItem->pNext = pFirst;
		pFirst = pItem;
	}
	void push(const T& value)
	{
		StackItem *pItem = new StackItem(value);
		pItem->pNext = pFirst;
		pFirst = pItem;
	}
	T pop()
	{
		if (pFirst != nullptr)
		{
			StackItem *p = pFirst;
			pFirst = pFirst->pNext;
			T item2 = p->item;
			delete p;
			return item2;
		}
		else throw out_of_range("Stack is empty");
	}
	const T& head() const
	{
		return pFirst->item;
	}

	template <typename ... Args>
	void push_emplace(Args&&... value) // ���������� ����� ���������� ��� � ������������ ������� T
	{
		StackItem *pItem = new StackItem(move(T(value...))); // �������� ���������� T(value...) � ������ ���������� � ������ �����������
		pItem->pNext = pFirst;
		pFirst = pItem;
	}
};

int main()
{
	int M1[] = { 1, 2, 3, 4, 5 };
	Massiv Ob1(M1, 5);
	mystack<Massiv> stack1;
	stack1.push_emplace(M1, 5);
	cout << "stack1.head()[0]=" << stack1.head()[0] << endl;
	cout << "stack1.head()[1]=" << stack1.head()[1] << endl;
	return 1;
}
